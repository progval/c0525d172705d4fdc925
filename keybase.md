### Keybase proof

I hereby claim:

  * I am ProgVal on github.
  * I am progval (https://keybase.io/progval) on keybase.
  * I have a public key whose fingerprint is 3790 43E3 DF96 D323 7E67  82AC 0E08 2B40 E437 6B1E

To claim this, I am signing this object:

```json
{
    "body": {
        "key": {
            "fingerprint": "379043e3df96d3237e6782ac0e082b40e4376b1e",
            "host": "keybase.io",
            "key_id": "0e082b40e4376b1e",
            "uid": "6d82134d7e95bb3c1744b2f600a7d400",
            "username": "progval"
        },
        "revoke": {
            "sig_ids": [
                "02f0d4f47dbfa7f727dc33a73b3da7ba9bbf50fcede25a015dd6516a613534cb0f"
            ]
        },
        "service": {
            "name": "github",
            "username": "ProgVal"
        },
        "type": "web_service_binding",
        "version": 1
    },
    "ctime": 1410528089,
    "expire_in": 157680000,
    "prev": "1d68b40c6dbcb0c05f6f87384d47d6512d6f0d2d84f6544e074a889b4f69ec53",
    "seqno": 5,
    "tag": "signature"
}
```

with the PGP key whose fingerprint is
[3790 43E3 DF96 D323 7E67  82AC 0E08 2B40 E437 6B1E](https://keybase.io/progval)
(captured above as `body.key.fingerprint`), yielding the PGP signature:

```
-----BEGIN PGP MESSAGE-----
Version: GnuPG v1

owFtUm1QVFUY3iVRWBIcQkIUGE7mB5Hc73t3WRliNGVCahJsoIWde+85d7kD3l13
YZX4mCZYKUcHyGmyhcBwEmhGi8qsJsWPwVKQwSlSgmIcWaMyxBXQCKmzZD+a6fw5
c97zPM/7PO+8DUsf0QXpw0OeSh/ji+P1vb9IupzIu4UVQLLDcmCqAMVo4VJUzYac
DqeqlQIToHkjwdCIhoqRgzRF84jjBUqUCUQIlMQQiKF5TiIRSAZFdleAgWUk0YU2
qHZcww+rCnH1f/BlCx8cFCiSZiCPjKwk0TLJM4xEKRxBiDxkCCIAdCGnJu5EGO1w
2m1usQRUJQMnctuLUcCxS7XhJi5gehkQlEJARmF4KCkir/AUD2WaFnlaoqHIS6JR
khSWUGQEEcWKBMlCyLEkJ3IkzdKMLBEKKMDauKFblRfEHza2qaVFZdJ/zbyAzez4
x0xpuSNQ2Y0k60OuVVI1iEeJKW7kdKl2DZhIjJRL1QCZZEiCpQRCMCYDtMehOpFV
DSBYnhMIfJJxVOTGkiTkBDw3mYMSticTrMIpAk8LDMQhsXcKcjgyBQVG4ViGQQTP
iIJglPDTiGSWBoE0uzQ7MLHYpmjDknhemlhaFuxEoMrwRtCqRTp9kG5xcFBgHXSG
0GX/7oiBiJgndtRED/e9el2zH5avuHK87RlPeK/qw5JWRI8VZsRzF3O1Cd3U4296
zZaBzo058UkZ9rc3XVR8EXMvjZ4drDUUToFp3Zab9UKOsTC9u+Q1iyfm+dDpVEfc
RPnqK7feGWzp8oe1hewZf7HtWJsSae0v74+py3K0r+47uT7vap///vWk3oR9sL0j
cefGgqRTPfp11bmd46NvHbmw37W/ddxWY/y9drzp+Mi5rD6u79khzzetq2pu0VP+
1tmZYN+Klk+rwxsitluA2fBXy7mmrV0JmbWb6upOHWlevHsbzLppns4/VOO5M6hF
Z5gnpr3mmkvWdTJ4sid7w28nHl3uudTRQd37/t7knfqe9ez2H8OHQ7po9yu76j2p
xysbfjralF9YvER9L+pC3kfLTq6l82yfXPN/HlQ35+v4Ie+xzowS/sOtJx4MxO71
Njf/mZ2pjKBFUbdnhLrICiXmRuyvn333+jNlP4e1nD7dXfF0820uauhAflpoz3Nf
Dvnnq6vONm6bn6/cPOAxpKydTJt89+sHH/BfMIOmiTNfXb6hc/rCjq6Zs4+GNmqd
5oZ+C53Sdj5mjOJD4ibJ2KI13edTCjY3tluMS3PjElcmVhafkbwrQ9MpfSajvR+T
+vHBP3ym6mOjS2Y9LQcOuh3Lfdlph+979iYkWb7122bL80buCpdn2sE1RxY93Ps3
=tW2D
-----END PGP MESSAGE-----

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/progval

### From the command line:

Consider the [keybase command line program](https://keybase.io/docs/command_line).

```bash
# look me up
keybase id progval

# encrypt a message to me
keybase encrypt progval -m 'a secret message...'

# ...and more...
```
